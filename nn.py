from sklearn.neighbors import NearestNeighbors
import numpy as np
import pandas as pd

def show_neighbors(index, n):
    mfccs = ["mfcc_mean_%i"%idx for idx in range(1,13)] + ["mfcc_var_%i"%idx for idx in range(1,13)]
    nbrs = NearestNeighbors(n_neighbors = n, algorithm='ball_tree').fit(df[mfccs].as_matrix())
    example  = df[mfccs].as_matrix()[index,:].reshape(1,-1) # reshape avoids a warning from sklearn wrt 1d arrays
    distances, indices = nbrs.kneighbors(example)
    for count, (distance, index) in enumerate(zip(distances[0], indices[0])):
        print('  %i: %.2f %s (%s)' % (count + 1, \
              distance,
              df.loc[index, 'name'],
              df.loc[index, 'path']))

df = pd.DataFrame.from_csv('database.csv')
df.describe() # Check statistics of descriptors
df.query("spectral_centroid < 50") # Show the files with very low centroid
df.ix[101][["name","tags","path"]] # Select one
show_neighbors(101, 10)
